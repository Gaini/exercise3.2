const express = require("express");
const app = express();
const rtAPIv1 = express.Router();

const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/taskMongo2';
var ObjectId = require('mongodb').ObjectId;

const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('невозможно подключиться к серверу MongoDB. Ошибка: ', err);
    } else {
        console.log('Соединение установлено для ', url);

        var collection = db.collection('phonebook');

        //Добавление записи - {"name": "Имя", "surname": "Фамилия", "phone": "777"}
        rtAPIv1.post('/phonebook', (req, res) => {
            usersObjects = {
                name: req.body.name,
                surname: req.body.surname,
                phone: req.body.phone
            };
            collection.insertOne(usersObjects, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.send(result.ops);
                }
            });
        }); //end of post, add new record

        //список всех записей
        rtAPIv1.get("/phonebook", function (req, res) {
            collection.find().toArray(function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.send(result);
                }

            });
        }); //end of get all records

        //удаление записи
        rtAPIv1.delete('/phonebook/:id', (req, res) => {
            collection.deleteOne({_id: new ObjectId(req.params.id)}, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.send(`Запись - ${req.params.id} удалена`);
                }
            });
        }); //end of delete record

        //редактирование записи - {"name": "Имя", "surname": "Фамилия", "phone": "777"}
        rtAPIv1.put('/phonebook/:id', (req, res) => {
            usersObjects = {
                name: req.body.name,
                surname: req.body.surname,
                phone: req.body.phone
            };
            collection.updateOne({_id: new ObjectId(req.params.id)}, {$set: usersObjects}, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    if (result) {
                        res.send(`Обновление успешно`);
                    } else {
                        res.status(400).send('Не найдены документы для обновления');
                    }
                }
            });

        });

        //Поиск записи {"name": "Имя", "surname": "Фамилия", "phone": "777"}
        rtAPIv1.post('/phonebook/search', (req, res) => {
            collection.find({$or: [{name: req.body.name}, {surname: req.body.surname}, {phone: req.body.phone}]})
                .toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.send(result);
                    }
                });
        }); //end of post, add new record

    } //end of else
});


app.listen(3000);
app.use("/api/v1", rtAPIv1);

