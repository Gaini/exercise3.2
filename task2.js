const express = require("express");
const app = express();
const rtAPIv1 = express.Router();

const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/taskMongo2';
var ObjectId = require('mongodb').ObjectId;

const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.set('views', __dirname + '/views')
app.set('view engine', 'jade')

MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('невозможно подключиться к серверу MongoDB. Ошибка: ', err);
    } else {
        console.log('Соединение установлено для ', url);
        var collection = db.collection('phonebook');
        /*
            Добавление записи - нажатие кнопки на главной
        */
        rtAPIv1.get('/phonebook/add', (req, res) => {
            res.render('add', { message: req.params.id});

        });
        /*
            Добавление записи в базу
        */
        rtAPIv1.post('/phonebook/add', (req, res) => {
            usersObjects = {
                name: req.body.username,
                surname: req.body.usersurname,
                phone: req.body.userphone
            };
            collection.insertOne(usersObjects, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/add","");
                    res.send(`Сохранено - <a href="${fullUrl}">Вернуться</a>`);
                }
            });
        }); //end of post, add new record

        /*
            Список всех записей
        */
        rtAPIv1.get("/phonebook", function (req, res) {
            collection.find({},{name: 1, surname: 1, phone:1, _id : true}).toArray(function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
                    result.forEach(function(item, i, arr) {
                        item.link = fullUrl + '/edit/' + item._id;
                        item.dellink = fullUrl + '/delete/' + item._id;
                        item.fullinfo = fullUrl + '/fullinfo/' + item._id;
                    });
                    res.render('index', { message: result, searchlink:fullUrl+'/search', addlink:fullUrl+'/add'});
                }

            });
        }); //end of get all records

        /*
            Удаление записи по ID
        */
        rtAPIv1.get('/phonebook/delete/:id', (req, res) => {
            collection.deleteOne({_id: new ObjectId(req.params.id)}, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/delete/"+req.params.id,"");
                    res.send(`Запись - ${req.params.id} удалена  - <a href="${fullUrl}">Вернуться</a>`);
                }
            });
        }); //end of delete record

        /*
            Редактирование записи - нажатие на ссылку на главной
         */
        rtAPIv1.get('/phonebook/edit/:id', (req, res) => {
            res.render('edit', { message: req.params.id});

        });
        /*
            Редактирование записи
         */
        rtAPIv1.post('/phonebook/edit/', (req, res) => {
            usersObjects = {
                name: req.body.username,
                surname: req.body.usersurname,
                phone: req.body.userphone
            };
            collection.updateOne({_id: new ObjectId(req.body.userid)}, {$set: usersObjects}, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    if (result) {
                        var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/edit/"+req.params.id,"");
                        res.send('Обновление успешно - <a href="'+fullUrl+'">Вернуться</a>');
                    } else {
                        res.status(400).send('Не найдены документы для обновления');
                    }
                }
            });

        });

        /*
            Полная информация
         */
        rtAPIv1.get('/phonebook/fullinfo/:id', (req, res) => {
            collection.find({_id: new ObjectId(req.params.id)})
                .toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/fullinfo/"+req.params.id,"");
                        res.render('showinfo', { message: result, link: fullUrl});
                    }
                });
        }); //end of post, add new record

        /*
            Поиск записи, нажатие кнопки на главной
         */
        rtAPIv1.get('/phonebook/search', (req, res) => {
            var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/search","");
            res.render('search', {message: '', link: fullUrl});
        });
        /*
            Поиск записи
         */
        rtAPIv1.post('/phonebook/search', (req, res) => {
            collection.find({$or: [{name: req.body.username}, {surname: req.body.usersurname}, {phone: req.body.userphone}]})
                .toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        var fullUrl = (req.protocol + '://' + req.get('host') + req.originalUrl).replace("/search","");
                        res.render('search', {message: result, link: fullUrl});
                    }
                });
        }); //end of post, add new record

    } //end of else
});

app.listen(3000);
app.use("/api/v1", rtAPIv1);

